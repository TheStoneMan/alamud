from .event import Event3

#Vider un récipient sur un objet.
class SplashWithEvent(Event3):
    NAME = "splash-with"

    def perform(self):
        #L'objet à vider doit déjà être rempli (filled) et l'objet sur lequel on vide doit être recouvrable (liquidable).
        if (not (self.object2.has_prop("filled") and self.object.has_prop("liquidable"))):
            self.fail()
            return self.inform("splash-with.failed")
        self.inform("splash-with")
