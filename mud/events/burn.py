from .event import Event3

#Brûler un objet avec un autre.
class BurnWithEvent(Event3):
    NAME = "burn-with"

    def perform(self):
        #Le second objet doit être enflammé, et le premier mouillé (on supposera recouvert de liquide inflammable).
        if (not (self.object2.has_prop("burning") and self.object.has_prop("wet"))):
            self.fail()
            return self.inform("burn-with.failed")
        self.inform("burn-with")
