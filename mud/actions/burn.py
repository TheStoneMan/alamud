from .action import Action3
from mud.events import BurnWithEvent

class BurnWithAction(Action3):
    EVENT = BurnWithEvent
    ACTION = "burn-with"
    RESOLVE_OBJECT2 = "resolve_for_use"
    RESOLVE_OBJECT = "resolve_for_surroundings"
