from .action import Action3
from mud.events import SplashWithEvent

class SplashWithAction(Action3):
    EVENT = SplashWithEvent
    ACTION = "splash-with"
    RESOLVE_OBJECT = "resolve_for_surroundings"
    RESOLVE_OBJECT2 = "resolve_for_use"
