from .action import Action1
from mud.events import WaitEvent

class WaitAction(Action1):
    EVENT = WaitEvent
    ACTION = "wait"
